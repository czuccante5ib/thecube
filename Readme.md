# App per simulare il lancio di un dado (cubo a 6 facce)

## Finalita'
L'applicazione si basa sull'utilizzo di piu' activity e la gestione
di esse passa attraverso l'utilizzo degli intent

## Funzionamento
Una volta avviata l'app cliccando sul bottone (Gioca) posizionato al centro del layout
dell'activity main viene avviata una activity 
secondaria. Nel layout dell'activity secondaria viene 
rappresentato in modo random un numero corrispondente a una faccia di un dado. Tramite
un secondo bottone (ancora) nell'activity secondaria si ritorna all'activity principale. 

## Procedura
Avviare android studio: Start new Android Studio project > Empty Activity. Per realizzare l'app saranno necessari i widget **Button** e **TextView**.  
 
**1.** Si crei un bottone (button1) sul layout dell'activity principale. 
Si fornisce del codice per raggiungere l'obiettivo. 
```
Button btn = (Button) findViewById(R.id.button);
btn.setOnClickListener(new View.OnClickListener() {
    public void onClick(View view) {
        // Toast.makeText(getBaseContext(), "Button was clicked!",
        //  Toast.LENGTH_SHORT).show();
        }
});  da
```
**2.**  Nell'**Activity** (secondaria) si utilizza la classe Random di java per generare i numeri corrispondenti a quelli riportati in una delle 6 faccie 
del dado.
Mediante il bottone (button1) (activity principale) viene dunque lanciata la seconda **Activity** 
utilizzando un **Intent** esplicito. 

Si fornisce del codice per raggiungere l'obiettivo. 
```
Intent i = new Intent(MainActivity.this, ActivityTwo.class);
startActivity(i);
```
Per consultare l'API cliccare 
[qui](https://developer.android.com/reference/android/content/Intent.html).
 
Nella seconda **Activity** va inserito oltre al widget **TextView** (numero uscito) anche un secondo bottone (button2) che consenta il ritorno all'activity principale.

Altre info sul progetto le troverete a breve sul file pdf allegato. 





